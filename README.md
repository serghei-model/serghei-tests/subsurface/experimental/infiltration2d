# Subsurface Test: 2D Infiltration

This is a 2D infiltration problem from Kirkland etal., (https://doi.org/10.1029/92WR00802). The soil is layered, which is good for testing model performance in heterogeneous geological conditions.


